﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    public GameObject playerPrefab;

    public static GameManager instance;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        if(PhotonNetwork.IsConnectedAndReady)
        {
            int randomSpawnPoint = Random.Range(0, 7);
            GameObject chosenSpawnPoint = SpawnPointHandler.instance.spawnPoints[randomSpawnPoint];

            PhotonNetwork.Instantiate(playerPrefab.name, new Vector3 (chosenSpawnPoint.transform.position.x, 0, chosenSpawnPoint.transform.position.z), Quaternion.identity);

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
