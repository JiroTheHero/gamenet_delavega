﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public string playerName;
    public int playerScore;

    public static PlayerStats instance;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

    }

    public List<PlayerStats> playerStatsList;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addScore()
    {
        playerScore++;
    }

    public PlayerStats(string newPlayerName, int newPlayerScore)
    {
        playerName = newPlayerName;
        playerScore = newPlayerScore;
    }
}
