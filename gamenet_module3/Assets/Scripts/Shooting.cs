﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Shooting : MonoBehaviourPunCallbacks
{
    public GameObject hitEffectPrefab;
    public GameObject bulletPrefab;
    public GameObject GunBarrel1;
    public GameObject GunBarrel2;
    public GameObject LaserBarrel;

    [Header("Gun Firing")]
    public float gunFireRate = 0.2f;
    private float gunFireTimer = 0;
    public float laserFireRate = 1f;
    private float laserFireTimer = 0;
    
    private bool isbuttonPressed = false;
    private bool isDead = false;
    public int DeathCount = 0;

    public bool isControlEnabled;

    [Header("HP related stuff")]
    public float startHealth = 100;
    public float health;
    public Image healthBar;
    
    [SerializeField] public static bool isGameOver = false;

    public enum RaiseEventsCode
    {
        WhoFinishedEventCode = 0
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code == (byte)RaiseEventsCode.WhoFinishedEventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;

            string nicknameOfEliminatedPlayer = (string)data[0];
            int viewId = (int)data[1];

            GameObject orderUiText = DeathRaceManager.instance.ElimTextUI;
            orderUiText.SetActive(true);

            if(viewId == photonView.ViewID) //this is u
            {
                orderUiText.GetComponent<Text>().text = "You were eliminated!";
                orderUiText.GetComponent<Text>().color = Color.red;
            }
            else
            {
                orderUiText.GetComponent<Text>().text = nicknameOfEliminatedPlayer + " has been eliminated!";
            }
        }

        if(DeathCount >= 2)
        {
                this.gameObject.GetComponent<PhotonView>().RPC("GameOver", RpcTarget.AllBuffered);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthBar = GameObject.Find("HealthBarFill").GetComponent<Image>();
        healthBar.fillAmount = health / startHealth;

        this.GetComponent<LineRenderer>().enabled = false;

        isControlEnabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (gunFireTimer < gunFireRate)
        {
            gunFireTimer += Time.deltaTime;
        }

        if (laserFireTimer < laserFireRate)
        {
            laserFireTimer += Time.deltaTime;
        }

        if(isControlEnabled)
        {
            if(Input.GetButton("Fire1") && gunFireTimer > gunFireRate)
            {
                gunFireTimer = 0.0f;
                FireGun();
            }

            if(Input.GetButton("Fire2"))
            {
                Debug.Log("Firing Lazer");
                laserFireTimer = 0.0f;
                isbuttonPressed = true;
                FireLaser();
            }
            else
            {
                isbuttonPressed = false;
            }
            
            this.gameObject.GetComponent<PhotonView>().RPC("EnableLineRenderer", RpcTarget.All, isbuttonPressed);
        }
    }

    public void FireLaser()
    {
        RaycastHit hit;
        Ray ray = new Ray(LaserBarrel.transform.position, LaserBarrel.transform.forward);
        
        if(Physics.Raycast(ray, out hit, 1000))
        {
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffect", RpcTarget.All, hit.point);
            gameObject.GetComponent<PhotonView>().RPC("RenderLaser", RpcTarget.All, hit.point);

            if(hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 0.2f);
            }
        }
    }
    
    void FireGun()
    {
        GameObject bullet1 = Instantiate(bulletPrefab, GunBarrel1.transform.position, GunBarrel1.transform.rotation);
        GameObject bullet2 = Instantiate(bulletPrefab, GunBarrel2.transform.position, GunBarrel2.transform.rotation);

        bullet1.GetComponent<Rigidbody>().velocity = GunBarrel1.transform.TransformDirection(Vector3.forward * 50);
        bullet2.GetComponent<Rigidbody>().velocity = GunBarrel1.transform.TransformDirection(Vector3.forward * 50);
    }
    
    public void Die()
    {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<PlayerSetup>().camera.transform.position = GameObject.Find("SpectatorPOV").GetComponent<Transform>().position;
        GetComponent<PlayerSetup>().camera.transform.rotation = GameObject.Find("SpectatorPOV").GetComponent<Transform>().rotation;

        float randomXPos = Random.Range(-10, 10);
        this.transform.position = GameObject.Find("DeathPoint").GetComponent<Transform>().position + new Vector3(randomXPos, 0, 0);

        GetComponent<VehicleMovement>().enabled = false;
        isControlEnabled = false;

        Text timerText = DeathRaceManager.instance.timerText;
        timerText.text = "Spectating";
        DeathCount++;
        PlayerEliminated();
        //photonView.RPC("AddDeathCount", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void TakeDamage(float damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if(health <= 0)
        {
            isDead = true;
            Die();
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
            
        }
    }

    [PunRPC]
    public void AddDeathCount()
    {
        
    }

    [PunRPC]
    public void CreateHitEffect (Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    [PunRPC]
    public void RenderLaser(Vector3 position)
    {
        this.GetComponent<LineRenderer>().SetWidth(0.2f, 0.2f);
        this.GetComponent<LineRenderer>().SetPosition(0, position);
        this.GetComponent<LineRenderer>().SetPosition(1, LaserBarrel.transform.position);
    }

    [PunRPC]
    public void EnableLineRenderer(bool buttonPressed)
    {
        if(buttonPressed)
        {
            this.GetComponent<LineRenderer>().enabled = true;
        }
        else
        {
            this.GetComponent<LineRenderer>().enabled = false;
        }
    }

    [PunRPC]
    public void GameOver()
    {
        GameObject winText = GameObject.Find("WinText");

        if(isDead)
        {
            winText.GetComponent<Text>().text = "You Lose. Try again next time.";
            winText.GetComponent<Text>().color = Color.red;
        }
        else
        {
            winText.GetComponent<Text>().text = "Congratulations! You Win!";
            winText.GetComponent<Text>().color = Color.green;
        }
    }

    public void PlayerEliminated()
    {
        string nickname = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        //event data
        object[] data = new object[] {nickname, viewId};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };
        
        PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.WhoFinishedEventCode, data, raiseEventOptions, sendOption);
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.CompareTag("Bullet"))
        {
            this.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 10.0f);
            Destroy(other.gameObject);
        }
    }
}
